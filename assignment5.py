#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_title = request.form['title']
        current_ids = [int(i['id']) for i in books]
        max_id = max(current_ids)
        ids_available = [True] * max_id

        # def assign(curr_id):
        #     ids_available[curr_id-1] = False
        # [assign(curr_id) for curr_id in current_ids]

        # map(lambda x: ids_available[x] = False, current_ids)
        # print(ids_available)

        for curr_id in current_ids:
            ids_available[curr_id-1] = False
        # print(ids_available)
        try:
            new_id = ids_available.index(True)+1
        except ValueError:
            new_id = max_id+1
        # for idx, available in enumerate(ids_available):
        #     if available == True:
        #         new_id = idx
        #         break
        new_book = {'title': new_title, 'id': new_id}
        books.insert(new_id-1, new_book)
        return redirect(url_for('showBook'))
    else:
	    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    book = books[0]
    for i in books:
        if int(i['id']) == int(book_id):
            book = i
            break
    if request.method == 'POST':
        new_title = request.form['title']
        book['title'] = new_title
        return redirect(url_for('showBook'))
    else:
	    return render_template('editBook.html', book = book)

	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    book = books[0]
    for i in books:
        if int(i['id']) == int(book_id):
            book = i
            break
    if request.method == 'POST':
        books.remove(book)
        return redirect(url_for('showBook'))
    else:
	    return render_template('deleteBook.html', book = book)

if __name__ == '__main__':
	app.debug = False
	app.run(host = '127.0.0.1', port = 5000)
	

